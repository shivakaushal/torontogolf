// Refresh on rotate
$( document ).ready(function() {

        $("#coltbtn").click(function () {
            var ref = 'colt';
            var old_url = location.href;
            var new_url = old_url.substring(0, old_url.indexOf('&'));
            location.href = new_url+'?ref='+ref;
   //         return false;
        });

         $("#watsonbtn").click(function () {
            var ref = 'watson';
            var old_url = location.href;
            var new_url = old_url.substring(0, old_url.indexOf('&'));
            location.href = new_url+'?ref='+ref;
//            return false;
        });


window.addEventListener("orientationchange", function() {
  var referrer =  document.referrer;

        if(referrer.toLowerCase().indexOf("colt") !== -1){
            var ref = 'colt';
        } else if (referrer.toLowerCase().indexOf("watson") !== -1){
            var ref = 'watson';
        }


        console.log(ref);
        var old_url = location.href;
        var new_url = old_url.substring(0, old_url.indexOf('&'));
        location.href = new_url+'?ref='+ref;

}, false);

});


console.log(window.orientation);

// Map Highlight
if (document.documentElement.clientWidth >= 769) {
    $('.map').mapster({
        stroke: false,
        fillColor: 'ffffff',
        fillOpacity: 0.4,
        clickNavigate: true
    });
}

// Scorecard switches
$("#coltbtn").click(function() {
    $(this).removeClass("inactive");
    $('#watsonbtn').addClass("inactive");
    $(".watsonscores").hide();
    $(".coltscores").show();
});
$("#watsonbtn").click(function() {
    $(this).removeClass("inactive");
    $('#coltbtn').addClass("inactive");
    $(".coltscores").hide();
    $(".watsonscores").show();
});


// Main tooltip goodies
var timeoutId;
yOffset = 155;
xOffset = 36;
ybtmOffset = -8;
xbtmOffset = 36;


if (document.documentElement.clientWidth >= 1025) {

    $('.tt, #tooltip').bind({
        mouseover: function() {
            clearInterval(timeoutId);
            $('area.tt').hover(function(e) {
                yloc = $(this).data('yloc');
                xloc = $(this).data('xloc');
                htitle = $(this).data('htitle');
                par = $(this).data('par');
                yards = $(this).data('yard');
                $(this).attr('id', htitle);
                link = 'hole/hole' + htitle + '.php';
                himage = 'img/thumbs/hole-' + htitle + '.png';
                $('#tooltip-btm').hide();
                $("#tooltip")
                    .attr('class', htitle)
                    .html('<img src="' + himage + '"><div class="details"><h2>HOLE ' + htitle + '</h2><h3 style="margin-bottom:0px;">PAR ' + par + '</h3><h3 style="margin-top:5px;">' + yards + ' yds</h3><p><a href="' + link + '" alt="View Hole">View Hole</a></p></div>')
                    .css('top', (yloc - yOffset) + 'px')
                    .css('left', (xloc - xOffset) + 'px')
                    .fadeIn(200);

            });
        },
        mouseleave: function() {
            timeoutId = setTimeout(function() {
                $('#tooltip').hide();
                $('#tooltip-left').hide();
            }, 0);
        }
    });

}



$('.tt-btm, #tooltip-btm').bind({
    mouseover: function() {
        clearInterval(timeoutId);
        $('area.tt-btm').hover(function(e) {
            yloc = $(this).data('yloc');
            xloc = $(this).data('xloc');
            htitle = $(this).data('htitle');
            par = $(this).data('par');
            yards = $(this).data('yard');
            $(this).attr('id', htitle);
            link = 'hole/hole' + htitle + '.php';
            himage = 'img/thumbs/hole-' + htitle + '.png';
            $('#tooltip').hide();
            $("#tooltip-btm")
                .attr('class', htitle)
                .html('<img src="' + himage + '"><div class="details"><h2>HOLE ' + htitle + '</h2><h3 style="margin-bottom:0px;">PAR ' + par + '</h3><h3 style="margin-top:5px;">' + yards + ' yds</h3><p><a href="' + link + '" alt="View Hole">View Hole</a></p></div>')
                .css('top', (yloc - ybtmOffset) + 'px')
                .css('left', (xloc - xbtmOffset) + 'px')
                .fadeIn(200);

        });
    },
    mouseleave: function() {
        timeoutId = setTimeout(function() {
            $('#tooltip-btm').hide();
            $('#tooltip-left').hide();
        }, 0);
    }
});



if (document.documentElement.clientWidth <= 1024) {

    // Map Highlight
    $('.map').mapster({
        stroke: false,
        fillColor: 'ffffff',
        fillOpacity: 0.4,
        clickNavigate: false,
        staticState: false
    });

    $("#masterwatson").on('click touch', function() {
        window.location = "http://torontogolf.coursetour.ca/watson/index.php";
    });

    $("#mastercolt").on('click touch', function() {
        window.location = "http://torontogolf.coursetour.ca/colt/index.php";
    });

    $('area.tt').bind("click touch", function() {
        yloc = $(this).data('yloc');
        xloc = $(this).data('xloc');
        htitle = $(this).data('htitle');
        par = $(this).data('par');
        yards = $(this).data('yard');

        var parent = $('.tt').closest('map').attr('id');

        if (parent == 'watson') {
            // Affordances for resize
            switch (htitle) {
                case 1:
                    yloc = 171;
                    xloc = 60;
                    break;
                case 3:
                    yloc = 239;
                    xloc = 172;
                    break;
                case 4:
                    yloc = 256;
                    xloc = 291;
                    break;
                case 5:
                    yloc = 290;
                    xloc = 297;
                    break;
                case 6:
                    yloc = 359;
                    xloc = 198;
                    break;
                case 7:
                    yloc = 506;
                    xloc = 323;
                    break;
                case 8:
                    yloc = 496;
                    xloc = 256;
                    break;
                case 9:
                    yloc = 410;
                    xloc = 102;
                    break;
            }
        } else if (parent == 'colt') {
            switch (htitle) {
                case 1:
                    yloc = 148;
                    xloc = 305;
                    break;
                case 2:
                    yloc = 145;
                    xloc = 126;
                    break;
                case 3:
                    yloc = 216;
                    xloc = 90;
                    break;
                case 4:
                    yloc = 373;
                    xloc = 105;
                    break;
                case 5:
                    yloc = 535;
                    xloc = 244;
                    break;
                case 6:
                    yloc = 456;
                    xloc = 441;
                    break;
                case 7:
                    yloc = 378;
                    xloc = 504;
                    break;
                case 8:
                    yloc = 403;
                    xloc = 378;
                    break;
                case 9:
                    yloc = 288;
                    xloc = 371;
                    break;
                case 10:
                    yloc = 260;
                    xloc = 545;
                    break;
                case 11:
                    yloc = 192;
                    xloc = 560;
                    break;
                case 12:
                    yloc = 213;
                    xloc = 624;
                    break;
                case 13:
                    yloc = 387;
                    xloc = 686;
                    break;
                case 14:
                    yloc = 479;
                    xloc = 606;
                    break;
                case 15:
                    yloc = 545;
                    xloc = 493;
                    break;
                case 16:
                    yloc = 465;
                    xloc = 254;
                    break;
                case 17:
                    yloc = 286;
                    xloc = 269;
                    break;
                case 18:
                    yloc = 143;
                    xloc = 414;
                    break;
            }
        }


        $(this).attr('id', htitle);
        link = 'hole/hole' + htitle + '.php';
        himage = 'img/thumbs/hole-' + htitle + '.png';
        $('#tooltip').hide();
        $('#tooltip-btm').hide();
        $('#tooltip-left').hide();
        if (htitle == 12 || htitle == 13 || htitle == 14) {
            yOffset = 145;
            xOffset = 215;
            $("#tooltip-left")
                .attr('class', htitle)
                .html('<img src="' + himage + '"><div class="details"><h2>HOLE ' + htitle + '</h2><h3 style="margin-bottom:0px;">PAR ' + par + '</h3><h3 style="margin-top:5px;">' + yards + ' yds</h3><p><a href="' + link + '" alt="View Hole">View Hole</a></p></div>')
                .css('top', (yloc - yOffset) + 'px')
                .css('left', (xloc - xOffset) + 'px')
                .fadeIn(200);
        } else {
            yOffset = 155;
            xOffset = 36;
            $("#tooltip")
                .attr('class', htitle)
                .html('<img src="' + himage + '"><div class="details"><h2>HOLE ' + htitle + '</h2><h3 style="margin-bottom:0px;">PAR ' + par + '</h3><h3 style="margin-top:5px;">' + yards + ' yds</h3><p><a href="' + link + '" alt="View Hole">View Hole</a></p></div>')
                .css('top', (yloc - yOffset) + 'px')
                .css('left', (xloc - xOffset) + 'px')
                .fadeIn(200);
        }

    });

    $('area.tt-btm').bind("click touch", function() {
        yloc = 57;
        xloc = 144;
        htitle = $(this).data('htitle');
        par = $(this).data('par');
        yards = $(this).data('yard');
        $(this).attr('id', htitle);
        link = 'hole/hole' + htitle + '.php';
        himage = 'img/thumbs/hole-' + htitle + '.png';
        $('#tooltip').hide();
        $('#tooltip-left').hide();
        $("#tooltip-btm")
            .attr('class', htitle)
            .html('<img src="' + himage + '"><div class="details"><h2>HOLE ' + htitle + '</h2><h3 style="margin-bottom:0px;">PAR ' + par + '</h3><h3 style="margin-top:5px;">' + yards + ' yds</h3><p><a href="' + link + '" alt="View Hole">View Hole</a></p></div>')
            .css('top', (yloc - ybtmOffset) + 'px')
            .css('left', (xloc - xbtmOffset) + 'px')
            .fadeIn(200);

    });


}


$('#tooltip-btm').hover(function(e) {
    ttid = $(this).attr('class');
    //timeoutId = setTimeout(function () {
    $('area#' + ttid).trigger("mouseover.mapster");
    // }, 0);
}).mouseleave(function(e) {
    //timeoutId = setTimeout(function () {
    $('area#' + ttid).trigger("mouseleave.mapster");
    $('#tooltip').hide();
    $('#tooltip-left').hide();
    //}, 0);
});


$('#tooltip').hover(function(e) {
    ttid = $(this).attr('class');
    //timeoutId = setTimeout(function () {
    $('area#' + ttid).trigger("mouseover.mapster");
    // }, 0);
}).mouseleave(function(e) {
    //timeoutId = setTimeout(function () {
    $('area#' + ttid).trigger("mouseleave.mapster");
    $('#tooltip-btm').hide();
    $('#tooltip-left').hide();
    //}, 0);
});

$('#tooltip-left').hover(function(e) {
    ttid = $(this).attr('class');
    //timeoutId = setTimeout(function () {
    $('area#' + ttid).trigger("mouseover.mapster");
    // }, 0);
}).mouseleave(function(e) {
    //timeoutId = setTimeout(function () {
    $('area#' + ttid).trigger("mouseleave.mapster");
    $('#tooltip-btm').hide();
    $('#tooltip').hide();
    //}, 0);
});


// Function to grab coords for tooltips
//$('.mainarea').mousemove(function(e){
//        var x = e.pageX - this.offsetLeft;
//        var y = e.pageY - this.offsetTop;
//       $('#coords').html("Y: " + y + " X: " + x);
//  });



function preload(arrayOfImages) {
    $(arrayOfImages).each(function() {
        $('area').attr('src', area.data("imgsrc")).appendTo('body').hide();
    });
}


$('.slidepopup').on('touchstart touchend tap click mouseenter', function(ev) {
    ev.preventDefault();
    if ($('.cam_information2').length > 0) {
        $('.cam_information2').toggleClass('cam_information2 cam_information');
    }
    var area = $(this);
    // set up the info text and position
    $('.cam_information').find('img:first').attr("src", area.data("imgsrc"));
    // get x,y coord for the popup
    var position = area.attr('coords').split(',');
    var x = position[0],
        y = position[1];
    var ImgSrc = $(this).attr('href');
    //prep slideshow
    for (var i = 0; i < imgArray.length; i++) {
        var v = imgArray[i];
        if (v == ImgSrc) {
            imgArray.splice(i, 1);
            i--;
        }
    }
    imgArray.unshift(ImgSrc);
    $('.cam_information').animate({
        left: x - 350,
        top: y - 260
    }, 250, function() {
        $('.cam_information').fadeIn('200');
    });
}).on('mouseleave', function() {
    $('.cam_information').fadeOut('200');
});


$('.slidepopdown').on('touchstart touchend tap click mouseenter', function(ev) {
    ev.preventDefault();
    if ($('.cam_information').length > 0) {
        $('.cam_information').toggleClass('cam_information cam_information2');
    }
    var area = $(this);
    // set up the info text and position
    $('.cam_information2').find('img:first').attr("src", area.data("imgsrc"));
    // get x,y coord for the popup
    var position = area.attr('coords').split(',');
    var x = position[0],
        y = position[1];
    var ImgSrc = $(this).attr('href');
    //prep slideshow
    for (var i = 0; i < imgArray.length; i++) {
        var v = imgArray[i];
        if (v == ImgSrc) {
            imgArray.splice(i, 1);
            i--;
        }
    }
    imgArray.unshift(ImgSrc);
    $('.cam_information2').animate({
        left: x - 350,
        top: y - 20
    }, 250, function() {
        $('.cam_information2').fadeIn('200');
    });
}).on('mouseleave', function() {
    $('.cam_information2').fadeOut('200');
});


if (navigator.userAgent.match(/iPad/i)) {
$("body").on('touchstart touchend tap click mouseenter', function(e) {
    if($(e.target).hasClass('zoom_icon')){
     console.log('Goodtimes');
   } else {
    $('.cam_information2').fadeOut('200');
    $('.cam_information').fadeOut('200');
}
});

$(".slidepopdown").click(function(e) {
    e.stopPropagation();
});

$("body").on('touchstart touchend tap click mouseenter', function(e) {
    if($(e.target).hasClass('zoom_icon')){
     console.log('Goodtimes');
   } else {
    $('.cam_information2').fadeOut('200');
    $('.cam_information').fadeOut('200');
}
});

$(".slidepopup").click(function(e) {
    e.stopPropagation();
});
}


$('.holepreview,.zoom_icon,.slidepopdown,.slidepopup').click(function(e) {
    e.preventDefault();
    $.fancybox(imgArray);
});

$('.cam_information2, .cam_information').on('mouseenter', function() {
    $(this).stop().fadeIn();
}).mouseleave(function() {
    $(this).fadeOut('600');
});

if (document.documentElement.clientWidth >= 1025) {

    $(window).resize(function() {

        $('.container').css({
            position: 'absolute',
            left: ($(window).width() - $('.container').outerWidth()) / 2,
            top: ($(window).height() - $('.container').outerHeight()) / 2
        });

    });

    $('.container').css({
        position: 'absolute',
        left: ($(window).width() - $('.container').outerWidth()) / 2,
        top: ($(window).height() - $('.container').outerHeight()) / 2
    });

}

$(window).resize(function() {

    $('.container').css({
        position: 'absolute',
        left: ($(window).width() - $('.container').outerWidth()) / 2,
        top: ($(window).height() - $('.container').outerHeight()) / 2
    });
});


// Bootstrap dropdown awesomeness
$('#holesMain').dropdown();



//Get correct size based on window size regardless


if (document.documentElement.clientWidth <= 1024) {
    $('#watsonmap').mapster('resize', 400);
}

if (document.documentElement.clientWidth >= 1025 && document.documentElement.clientWidth <= 1200) {
    $('#mainmap').mapster('resize', 760);
    $('#coltmap').mapster('resize', 760);
    $('#watsonmap').mapster('resize', 444);
}

if (document.documentElement.clientWidth > 1200) {
    $('#mainmap').mapster('resize', 788);
    $('#coltmap').mapster('resize', 788);
    $('#watsonmap').mapster('resize', 444);
}



// Resize on window resize
$(window).resize(function() {

    if (document.documentElement.clientWidth > 1200) {
        $('#mainmap').mapster('resize', 788);
        $('#coltmap').mapster('resize', 788);
        $('#watsonmap').mapster('resize', 444);
    }

    if (document.documentElement.clientWidth <= 1024) {
        $('#watsonmap').mapster('resize', 400);
    }

    if (document.documentElement.clientWidth > 1200) {
        $('#watsonmap').mapster('resize', 444);
    }

    if (document.documentElement.clientWidth >= 1025 && document.documentElement.clientWidth <= 1200) {
        $('#mainmap').mapster('resize', 760);
        $('#coltmap').mapster('resize', 760);
        $('#watsonmap').mapster('resize', 444);
    }

});





if (navigator.userAgent.match(/iPad/i)) {
    var viewportmeta = document.querySelector('meta[name="viewport"]');
    if (viewportmeta) {
        viewportmeta.content = 'width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0';
        document.body.addEventListener('gesturestart', function() {
            viewportmeta.content = 'width=device-width, minimum-scale=0.25, maximum-scale=1.6';
        }, false);
    }
}

// Some filename pulling
var filename = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
//var filename = filename.substring(0 , filename.indexOf('?'));

var filenamen = filename.indexOf('?');
filename = filename.substring(0, filenamen != -1 ? filenamen : filename.length);
var holeNameforNav = filename.replace('e', 'e ');
var holeNameforNav = holeNameforNav.slice(0, -4);
var holeNameforNav = holeNameforNav.charAt(0).toUpperCase() + holeNameforNav.slice(1);
$('.mobilenavbtn').html(holeNameforNav + ' <img src="/img/arrow-dropdown.png">');
