<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/templates/header.php";
   include_once($path);
?>

      <div class="row-fluid">

        <div class="span3 hidden-phone hidden-tablet">
            <div class="row-fluid">
              <div class="sidebar logo">
                <a href="/">
                  <img src="/img/toronto-golf.png">
                </a>
                <div class="coursename">
                  <h3>Watson<br/>Course</h3>
                </div>
              </div>
            </div>

        <div class="bottomnav">
          <div class="sticky">
            <div class="row-fluid buttonnav">
              <div class="sidebar">
                <a class="btn-new" href="/"><img src="/img/main-icon.png">Main Map</a>
                <a class="btn-new"  style="margin-bottom:0" href="/scorecard.php"><img src="/img/scorecard-icon.png">Scorecard</a>
              </div>
            </div>

              <div class="row-fluid">
                  <p class="copyright">Copyright &copy; 2016 <a href="http://t2greengolf.ca">POWERED BY T2GREEN</a></p>
              </div>
        </div>
      </div>
    </div>

        <div class="span9">
          <div class="row-fluid">
              <div class="hidden-desktop">
              <div class="nav">
                    <a class="btn-new" href="/index.php"><img src="/img/house-icon-small.png"></a>
                    <div class="holesDropdown" style="position:relative;">
                    <a class="btn-new" id="holesMain" data-toggle="dropdown" href="#">Hole &nbsp;&nbsp;&nbsp;<img src="/img/arrow-dropdown.png"></a>
                    <?php include_once('hole/dropdown.php'); ?>
                    </div>
                    <a class="btn-new inactive"><img src="/img/prev.png"></a>
                    <a class="btn-new" style="margin-right:0px !important;" href="hole/hole1.php"><img src="/img/next.png"></a>
              </div>

            <div class="mobile-course-header">
              <img class="watson" src="../img/toronto-golf.png">
              <h2>Watson Course</h2>
            </div>

            <img id="watsonmap" class="mobilemap" src="../img/watson-map.jpg">
            <a class="btn-new scorecard-btn" href="/scorecard.php"><img src="/img/scorecard-icon.png">Scorecard</a>
          </div>

           <div class="hidden-phone hidden-tablet">
          <div class="mainarea" id="watsoncont" style="margin-left:200px;">

            <div id="tooltip"></div>
            <div id="tooltip-btm"></div>
            <img class="map" id="watsonmap" src="/img/watson-map.jpg" usemap="#watson">
            <map name="watson" id="watson">
              <area class="tt" shape="poly" coords="66,291,59,291,53,288,47,280,44,268,43,256,42,245,40,238,34,226,33,219,34,213,38,200,40,184,39,177,36,170,33,163,33,154,37,123,37,107,35,95,34,86,37,77,44,69,53,64,58,62,62,58,70,46,78,28,80,25,84,21,92,18,104,16,126,12,130,13,132,18,136,35,136,42,132,47,117,54,110,59,104,68,97,83,96,88,96,96,95,103,92,112,82,135,73,160,72,171,74,180,77,191,79,206,80,243,78,261,75,276,71,287,66,291,66,291"
                href="hole/hole1.php" data-yloc="95" data-xloc="65" data-htitle="1" data-par="4" data-yard="376" >
              <area class="tt-btm" shape="poly" coords="139,130,136,132,131,133,124,132,118,127,106,113,103,106,104,102,107,98,115,92,121,90,125,87,128,79,129,62,130,56,133,50,141,38,143,31,142,24,137,13,138,10,144,9,152,9,157,11,159,15,159,22,160,30,162,36,167,43,169,49,170,59,169,71,166,75,163,79,156,87,151,98,139,130,139,130"
                href="hole/hole2.php" data-yloc="68" data-xloc="150" data-htitle="2" data-par="3" data-yard="146">
              <area class="tt" shape="poly" coords="160,272,162,263,160,253,155,239,152,231,150,222,150,199,149,168,144,127,146,117,152,106,156,103,161,102,165,104,169,110,171,118,172,125,171,141,172,151,175,164,181,180,190,201,194,213,196,229,199,253,205,292,206,306,205,320,201,331,198,334,194,335,183,333,173,327,165,319,163,313,163,307,164,296,164,288,160,272,160,272"
                href="hole/hole3.php" data-yloc="250" data-xloc="174" data-htitle="3" data-par="4" data-yard="299">
              <area class="tt" shape="poly" coords="269,330,264,331,261,330,259,324,261,313,267,301,276,289,284,279,288,274,291,266,296,247,300,231,301,226,303,225,311,225,321,222,330,222,333,223,334,226,340,247,341,257,340,260,336,263,327,267,321,273,312,289,306,299,298,309,286,319,269,330,269,330"
                href="hole/hole4.php" data-yloc="280" data-xloc="300" data-htitle="4" data-par="3" data-yard="144">
              <area class="tt" shape="poly" coords="296,370,288,375,279,378,268,378,259,375,255,370,255,364,258,360,273,339,289,320,329,294,351,279,362,268,368,261,372,257,379,255,386,256,388,259,389,263,385,274,376,288,366,301,357,309,331,333,296,370,296,370"
                href="hole/hole5.php" data-yloc="314" data-xloc="323" data-htitle="5" data-par="3" data-yard="204">
              <area class="tt" shape="poly" coords="219,478,214,481,205,482,196,481,194,478,192,474,194,458,197,439,202,415,195,371,193,353,195,337,200,332,208,329,213,329,217,331,221,335,223,341,225,379,226,402,228,420,230,436,230,452,226,467,219,478,219,478"
                href="hole/hole6.php" data-yloc="395" data-xloc="205" data-htitle="6" data-par="3" data-yard="179">
              <area class="tt" shape="poly" coords="243,484,235,477,232,470,232,464,237,452,242,450,250,455,268,469,294,487,333,512,344,526,363,554,382,583,391,594,397,601,408,610,416,623,420,634,420,642,416,648,410,656,406,658,401,659,396,658,390,654,382,645,372,631,351,595,332,561,320,544,291,515,273,497,262,489,251,487,243,484,243,484"
                href="hole/hole7.php" data-yloc="575" data-xloc="358" data-htitle="7" data-par="4" data-yard="358">
              <area class="tt" shape="poly" coords="286,552,292,573,295,588,295,595,293,599,282,609,276,610,271,607,267,594,264,576,259,549,249,529,245,516,245,511,247,507,259,501,265,501,270,502,277,504,283,509,288,515,290,525,287,534,284,539,283,544,286,552,286,552"
                href="hole/hole8.php" data-yloc="550" data-xloc="268" data-htitle="8" data-par="3" data-yard="121">
              <area class="tt" shape="poly" coords="227,512,185,508,133,504,108,502,94,499,86,494,84,488,84,478,87,465,91,460,96,457,104,458,114,462,137,471,173,479,224,489,230,492,235,499,236,502,235,506,232,510,227,512,227,512"
                href="hole/hole9.php" data-yloc="488" data-xloc="150" data-htitle="9" data-par="3" data-yard="185">
            </map>

         </div>
        </div>

      </div>
    </div>

<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/templates/footer-holes.php";
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/templates/footer.php";
   include_once($path);
?>
