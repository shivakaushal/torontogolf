<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/templates/header.php";
   include_once($path);
?>

      <div class="row-fluid">

        <div class="span3 hidden-phone hidden-tablet">
            <div class="row-fluid">
              <div class="sidebar logo">
                <a href="/">
                  <img src="/img/toronto-golf.png">
                </a>
                <div class="coursename">
                  <h3>Watson<br/>Course</h3>
                </div>
              </div>
            </div>

        <div class="bottomnav">
          <div class="sticky">
            <div class="row-fluid buttonnav">
              <div class="sidebar">
                <a class="btn-new" href="/"><img src="/img/main-icon.png">Main Map</a>
                <a class="btn-new"  style="margin-bottom:0" href="/scorecard.php"><img src="/img/scorecard-icon.png">Scorecard</a>
              </div>
            </div>

              <div class="row-fluid">
                  <p class="copyright">Copyright &copy; 2013 <a href="#">POWERED BY T2GREEN</a></p>
              </div>
        </div>
      </div>
    </div>

        <div class="span9">
          <div class="row-fluid">
              <div class="hidden-desktop">
              <div class="nav">
                    <a class="btn-new" href="/index.php"><img src="/img/house-icon-small.png"></a>
                    <div class="holesDropdown" style="position:relative;">
                    <a class="btn-new" id="holesMain" data-toggle="dropdown" href="#">Hole &nbsp;&nbsp;&nbsp;<img src="/img/arrow-dropdown.png"></a>
                    <?php include_once('hole/dropdown.php'); ?>
                    </div>
                    <a class="btn-new inactive"><img src="/img/prev.png"></a>
                    <a class="btn-new" style="margin-right:0px !important;" href="hole/hole1.php"><img src="/img/next.png"></a>
              </div>

            <div class="mobile-course-header">
              <img class="watson" src="../img/toronto-golf.png">
              <h2>Watson Course</h2>
            </div>

            <img id="watsonmap" class="mobilemap" src="../img/watson-map.jpg">
            <a class="btn-new scorecard-btn" href="/scorecard.php"><img src="/img/scorecard-icon.png">Scorecard</a>
          </div>

           <div class="hidden-phone hidden-tablet">
          <div class="mainarea" id="watsoncont" style="margin-left:200px;">

            <div id="tooltip"></div>
            <div id="tooltip-btm"></div>
            <img class="map" id="watsonmap" src="/img/watson-map.jpg" usemap="#watson">
            <map name="watson" id="watson">
  <area class="tt" shape="poly" coords="86,358,80,352,74,339,71,318,70,300,67,280,65,261,62,240,61,227,58,216,55,198,52,180,49,160,44,140,41,126,39,110,37,94,37,88,36,80,38,66,40,52,49,50,54,52,58,57,60,64,61,72,61,87,61,96,62,103,64,108,64,108,68,111,74,116,76,125,76,134,76,145,80,156,83,167,88,184,88,196,90,216,90,227,96,238,98,253,98,269,98,283,102,302,102,314,102,328,100,337,100,342,100,343,95,349,92,354" href="hole/hole1.php" data-yloc="191" data-xloc="71" data-htitle="1" data-par="5" data-yard="436" >
  <area class="tt-btm" shape="poly" coords="50,44,42,44,34,37,30,26,37,22,47,22,60,26,72,26,86,29,94,30,103,32,114,34,126,38,134,41,140,42,149,45,160,46,166,48,172,50,176,52,178,58,179,63,179,74,179,84,179,96,179,112,179,118,179,130,179,142,177,152,171,154,163,154,156,148,154,139,150,130,150,121,145,120,144,120,136,114,135,107,134,97,134,89,135,84,140,76,134,75,126,75,118,74,111,70,102,66,96,62,90,58,87,56,76,52,71,51,62,50,56,48" href="hole/hole2.php" data-yloc="64" data-xloc="159" data-htitle="2" data-par="4" data-yard="310">
  <area class="tt" shape="poly" coords="130,154,127,148,122,135,126,128,133,128,139,132,144,142,152,154,156,158,164,168,168,181,175,192,183,206,188,218,194,234,200,248,204,258,205,269,208,280,208,298,208,312,208,326,204,337,200,350,186,352,177,344,176,333,176,321,178,305,178,291,180,281,180,270,178,261,178,258,175,250,172,242,170,238,168,234,163,229,161,224,157,218,154,210,148,200,144,186,143,182,138,168" href="hole/hole3.php" data-yloc="265" data-xloc="189" data-htitle="3" data-par="4" data-yard="328">
  <area class="tt" shape="poly" coords="286,327,278,323,280,310,290,302,299,291,307,283,314,275,323,265,328,254,338,246,349,244,358,244,362,249,364,258,364,266,357,274,348,284,338,292,326,295,320,300,312,307,304,310,300,314,298,320,290,324" href="hole/hole4.php" data-yloc="285" data-xloc="324" data-htitle="4" data-par="3" data-yard="149">
  <area class="tt" shape="poly" coords="386,280,396,272,393,262,383,264,370,274,357,280,353,287,344,295,336,300,326,308,318,310,310,315,304,322,298,333,292,340,285,348,278,356,274,360,274,370,280,374,288,375,295,373,302,366,308,356,315,350,318,342,330,332,337,326,344,319,350,312,362,300,370,292,379,286" href="hole/hole5.php" data-yloc="319" data-xloc="328" data-htitle="5" data-par="3" data-yard="212">
  <area class="tt" shape="poly" coords="209,340,211,332,213,322,220,322,225,332,226,340,226,356,227,365,227,376,227,390,227,406,227,418,228,422,230,428,233,441,233,452,231,464,225,472,216,472,206,465,204,452,204,432,206,418,206,404,208,390,210,380,210,367,210,354,210,347" href="hole/hole6.php" data-yloc="401" data-xloc="220" data-htitle="6" data-par="3" data-yard="192">
  <area class="tt" shape="poly" coords="263,485,259,478,252,473,252,464,253,458,260,460,265,460,270,464,276,466,284,474,292,482,301,490,316,496,327,502,335,508,340,514,348,525,357,536,364,546,372,558,379,570,384,578,390,591,394,596,397,600,403,606,413,610,424,612,426,620,426,628,425,640,420,642,414,644,406,644,398,642,392,640,387,631,381,616,379,608,373,596,368,588,359,579,347,564,340,560,330,544,320,536,310,520,304,514,297,506,291,498,281,493,275,489" href="hole/hole7.php" data-yloc="560" data-xloc="358" data-htitle="7" data-par="4" data-yard="350">
  <area class="tt" shape="poly" coords="290,600,282,596,276,590,273,578,273,572,275,562,276,556,275,550,275,541,271,534,267,528,262,517,262,510,265,504,270,500,276,500,282,501,286,502,293,506,293,512,293,522,292,534,292,540,292,546,293,551,298,558,301,572,301,580,300,590,297,597" href="hole/hole8.php" data-yloc="550" data-xloc="284" data-htitle="8" data-par="3" data-yard="127">
  <area class="tt" shape="poly" coords="226,486,232,487,239,491,238,500,221,501,199,501,187,499,168,494,150,490,130,486,118,480,105,473,101,466,98,455,96,439,94,429,92,420,89,407,86,399,85,392,85,385,85,375,88,373,92,368,98,366,106,364,115,364,118,372,122,382,122,392,120,400,119,413,120,425,120,429,124,436,127,443,132,452,139,456,147,460,155,462,169,468,179,470,186,472,195,474,205,477,212,478,220,480" href="hole/hole9.php" data-yloc="458" data-xloc="114" data-htitle="9" data-par="4" data-yard="319">
</map>

         </div>
        </div>

      </div>
    </div>

<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/templates/footer-holes.php";
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/templates/footer.php";
   include_once($path);
?>
