<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>The Toronto Golf Club</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <script type="text/javascript" src="//use.typekit.net/xij4aoj.js"></script>
    <script type="text/javascript">try{Typekit.load();}catch(e){}</script>

    <!-- Le styles -->
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/bootstrap-responsive.css" rel="stylesheet">
    <link href="/css/main.css" rel="stylesheet">

    <link rel="stylesheet" href="fancybox/source/jquery.fancybox.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="fancybox/source/helpers/jquery.fancybox-buttons.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="fancybox/source/helpers/jquery.fancybox-thumbs.css" type="text/css" media="screen" />

    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="/js/html5shiv.js"></script>
    <![endif]-->
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
  </head>

  <body>
    <!--[if lte IE 8]>
        <div id="oldoverlay">
            <div class="topoverlay">
            <h2>Browser Update Required</h2>
            <p>Please update to one of these browsers before viewing the course tour.</p>
            <div class="buttonsover">
                <a href="http://www.google.com/chrome"><img style="float:left; margin-right:40px; margin-left:10px;" src="/img/chrome.png"></a>
                <a href="http://www.firefox.com"><img style="float:left;" src="/img/firefox.png"></a>
            </div>
            </div>
        </div>
    <![endif]-->
    <div class="wrapper">
    <div class="container">
