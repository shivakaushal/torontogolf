
   </div> <!-- /container -->
   <div class="row-fluid hidden-desktop visible-tablet visible-phone" style="margin:0px auto 10px; display:block;">
        <div class="span12">
            <p class="copyright">Copyright &copy; 2016 <a href="http://www.golfyardageguides.com/">POWERED BY T2GREEN</a></p>
        </div>
    </div>
 </div>

    <script src="/js/bootstrap.js"></script>
    <!-- <script src="/js/scale.js"></script>-->
    <script src="/js/mapster.js"></script>
    <!-- Add fancyBox -->
    <script type="text/javascript" src="/fancybox/source/jquery.fancybox.pack.js"></script>
    <script type="text/javascript" src="/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
    <!-- Optionally add helpers - button, thumbnail and/or media -->

    <script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-buttons.js"></script>
    <script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-media.js"></script>
    <script type="text/javascript" src="/fancybox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <script type="text/javascript" src="/js/main.js"></script>
  </body>
</html>
