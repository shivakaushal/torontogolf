<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/templates/header.php";
   include_once($path);
?>

      <div class="row-fluid">

        <div class="span3 hidden-phone hidden-tablet">
            <div class="row-fluid">
              <div class="sidebar logo">
                <a href="/">
                  <img src="/img/toronto-golf.png">
                </a>
              </div>
            </div>

        <div class="bottomnav">
          <div class="sticky">
            <div class="row-fluid buttonnav">
              <div class="sidebar">
                <a class="btn-new" href="/"><img src="/img/main-icon.png">Main Map</a>
              </div>
            </div>

              <div class="row-fluid">
                  <p class="copyright">Copyright &copy; 2016 <a href="#">POWERED BY T2GREEN</a></p>
              </div>
        </div>
      </div>
    </div>

        <div class="span9">
          <div class="mainarea">
            <div class="btmgrad score-header hidden-phone hidden-tablet">
              <h2 style="letter-spacing:2px;">Scorecard</h2>
            </div>

            <div class="score-buttons hidden-phone hidden-tablet">
              <ul>
                <li><a href="#" class="btn-new" id="coltbtn">Colt Course</a></li>
                <li><a class="inactive btn-new" href="#" id="watsonbtn">Watson Course</a></li>
              </ul>
            </div>

            <div class="hidden-desktop visible-phone visible-tablet phone-header">
             <a class="btn-new scoreback" onclick="history.back(-1)"><img src="/img/prev.png"></a>
                <div class="coursename">
                  <div class="scorewrapper">
                    <h3 id="watsonhead">Watson Course</h3>
                    <h2 id="scorehead" style="letter-spacing:2px;">Scorecard</h2>
                  </div>
                </div>
            </div>


    <div class="scorecard">

      <table cellpadding="0" class="watsonscores">
        <tr class="tableClass-b tableClass-b3">
          <td></td>
          <td><div class="labelBlue"></div></td>
          <td><div class="labelWhite"></div></td>
          <td><div class="labelRed"></div></td>
          <td><div class="labelYellow"></div></td>
          <td>PAR</td>
        </tr>
        <tr class="tableClass-b">
          <td>1</td>
          <td>376</td>
          <td>326</td>
          <td>302</td>
          <td>228</td>
          <td>4</td>

        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>2</td>
          <td>146</td>
          <td>135</td>
          <td>92</td>
          <td>92</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b">
          <td>3</td>
          <td>299</td>
          <td>290</td>
          <td>261</td>
          <td>190</td>
          <td>4</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>4</td>
          <td>144</td>
          <td>120</td>
          <td>103</td>
          <td>103</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b">
          <td>5</td>
          <td>204</td>
          <td>171</td>
          <td>133</td>
          <td>133</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>6</td>
          <td>179</td>
          <td>162</td>
          <td>86</td>
          <td>86</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b">
          <td>7</td>
          <td>358</td>
          <td>314</td>
          <td>308</td>
          <td>159</td>
          <td>4</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>8</td>
          <td>121</td>
          <td>110</td>
          <td>100</td>
          <td>42</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b">
          <td>9</td>
          <td>185</td>
          <td>175</td>
          <td>164</td>
          <td>127</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-d">
          <td>Tot</td>
          <td>2012</td>
          <td>1803</td>
          <td>1549</td>
          <td>1160</td>
          <td>30</td>
        </tr>
      </table>


    </div>
          </div>
        </div>

      </div>

<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/templates/footer.php";
   include_once($path);
?>
