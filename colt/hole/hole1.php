<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/templates/header.php";
   include_once($path);
?>
        <div class="row">
            <div class="span6">
                <?php include_once('mobile-nav.php'); ?>
                <div class="row">
                    <div class="sidebar logo">
                        <a href="/"><img class="pull-left holelogo" src="/img/toronto-golf.png"></a>
                        <div class="coursedetails">
                            <h3>
                                Hole 1
                            </h3>
                            <h4>
                                First
                            </h4>
                            <h5>
                                Colt Course
                            </h5>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="topgrad"></div>
                            <div class="nav">
                                <a class="btn-new" href="/colt/index.php"><img src="/img/main-icon.png">Map</a>
                                <div class="holesDropdown" style="position:relative;">
                                    <a class="btn-new" id="holesMain" data-toggle="dropdown" href="#">Hole 1 <img src="/img/arrow-dropdown.png"></a>         <ul class="dropdown-menu" role="menu" aria-labelledby="holesMain">
            <li>
                <a href="/colt/hole/hole1.php">Hole 1</a>
            </li>
            <li>
                <a href="/colt/hole/hole2.php">Hole 2</a>
            </li>
            <li>
                <a href="/colt/hole/hole3.php">Hole 3</a>
            </li>
            <li>
                <a href="/colt/hole/hole4.php">Hole 4</a>
            </li>
            <li>
                <a href="/colt/hole/hole5.php">Hole 5</a>
            </li>
            <li>
                <a href="/colt/hole/hole6.php">Hole 6</a>
            </li>
            <li>
                <a href="/colt/hole/hole7.php">Hole 7</a>
            </li>
            <li>
                <a href="/colt/hole/hole8.php">Hole 8</a>
            </li>
            <li>
                <a href="/colt/hole/hole9.php">Hole 9</a>
            </li>
            <li>
                <a href="/colt/hole/hole10.php">Hole 10</a>
            </li>
            <li>
                <a href="/colt/hole/hole11.php">Hole 11</a>
            </li>
            <li>
                <a href="/colt/hole/hole12.php">Hole 12</a>
            </li>
            <li>
                <a href="/colt/hole/hole13.php">Hole 13</a>
            </li>
            <li>
                <a href="/colt/hole/hole14.php">Hole 14</a>
            </li>
            <li>
                <a href="/colt/hole/hole15.php">Hole 15</a>
            </li>
            <li>
                <a href="/colt/hole/hole16.php">Hole 16</a>
            </li>
            <li>
                <a href="/colt/hole/hole17.php">Hole 17</a>
            </li>
            <li>
                <a href="/colt/hole/hole18.php">Hole 18</a>
            </li>
        </ul>
                                </div><a class="btn-new inactive"><img src="/img/prev.png"></a> <?php
                                $pinfo = pathinfo($_SERVER["SCRIPT_FILENAME"]);
                                $reqpath = dirname($_SERVER["REQUEST_URI"]);

                                if(preg_match("/(.*?)(\d+)\.php/",  $pinfo["basename"], $matches)) {
                                    $fnbase = $matches[1];
                                    $fndir = $pinfo["dirname"];
                                    $current = intval($matches[2]);
                                    $next = $current + 1;
                                    $prior = $current - 1;
                                    $next_file = $fndir . DIRECTORY_SEPARATOR . $fnbase . $next . ".php";
                                    $prior_file = $fndir . DIRECTORY_SEPARATOR . $fnbase . $prior . ".php";

                                    if(!file_exists($next_file)) $next_file = false;
                                    if(!file_exists($prior_file)) $prior_file = false;


                                    if($prior_file) {
                                        $link = $reqpath . DIRECTORY_SEPARATOR . basename($prior_file);

                                        echo '<a href="'.$link.'" class="btn-new"><img src="/img/prev.png"></a>';
                                    }

                                    if($next_file) {
                                        $link = $reqpath . DIRECTORY_SEPARATOR . basename($next_file);


                                        echo '<a class="btn-new" href="'.$link.'"><img src="/img/next.png"></a>';
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="row">
                            <div class="holedetails">
                                <div class="hole_par">
                                    <h2>
                                        Par 4
                                    </h2>
                                    <div class="yards">
                                        <div class="yard">
                                            <div class="yrdBlack"></div>
                                            <p>
                                                370
                                            </p>
                                        </div>
                                        <div class="yard">
                                            <div class="yrdBlue"></div>
                                            <p>
                                                370
                                            </p>
                                        </div>
                                        <div class="yard">
                                            <div class="yrdWhite"></div>
                                            <p>
                                                354
                                            </p>
                                        </div>
                                        <div class="yard">
                                            <div class="yrdRed"></div>
                                            <p>
                                                338
                                            </p>
                                        </div>
                                        <div class="yard">
                                            <div class="yrdGreen"></div>
                                            <p>
                                                322
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="hidden-desktop mobile-hole">
                                    <img src="../img/hole01.jpg">
                                </div>
                                <div class="description">
                                    <p>
                                        Comfortable opening dogleg opening hole which favours the right hand side of the fairway for an optimum approach shot to the green. But not too far right.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <p class="copyright" style="margin-left:30px;">
                                Copyright © 2016 <a target="_blank" href="http://www.golfyardageguides.com/">POWERED BY T2GREEN</a>
                            </p>
                            <p class="instructions">
                                <img style="width:20px; height: auto;" src="/img/camera-cursor.png"> Hover over a camera point for photos.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="span6">
                <div class="mainarea">
                    <img name="hole01_icons" src="../img/hole01_icons.jpg" id="cc_hole1" usemap="#m_cc_hole1" alt=""> <map name="m_cc_hole1" id="cc_hole12">
                        <area shape="poly" class="slidepopup" coords="243,620,275,620,275,641,243,641,243,620" href="../img/slides/01-1.jpg" alt="" data-imgsrc="../img/slides/01-1-thumb.jpg">
                        <area shape="poly" class="slidepopdown" coords="380,158,412,158,412,179,380,179,380,158" href="../img/slides/01-2.jpg" alt="" data-imgsrc="../img/slides/01-2-thumb.jpg">
                    </map>
                    <div class="cam_information">
                        <!-- POPUP CONTENT -->
                        <img src="" alt="" class="holepreview">
                        <div class="zoom_icon"></div>
                    </div><script type="text/javascript">
var imgArray = ['../img/slides/01-1.jpg', '../img/slides/01-2.jpg'];
                    </script>
                </div>
            </div>
        </div><?php
           $lead = $_SERVER['DOCUMENT_ROOT'];
           $lead .= "/templates/footer-holes.php";
           include_once($lead);
           $path = $_SERVER['DOCUMENT_ROOT'];
           $path .= "/templates/footer.php";
           include_once($path);
        ?>
