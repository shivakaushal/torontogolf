<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/templates/header.php";
   include_once($path);
?>

      <div class="row">

        <div class="span6">
          <?php include_once('mobile-nav.php'); ?>
            <div class="row">
              <div class="sidebar logo">
                <a href="/">
                  <img class="pull-left holelogo" src="/img/toronto-golf.png">
                </a>
                <div class="coursedetails">
                  <h3>Hole 2</h3>
                  <h4>North</h4>
                  <h5>Colt Course</h5>
                </div>

                <div class="clearfix"></div>

              <div class="row">
                <div class="topgrad"></div>
                <div class="nav">
                    <a class="btn-new" href="/colt/index.php"><img src="/img/main-icon.png">Map</a>
                    <div class="holesDropdown" style="position:relative;">
                    <a class="btn-new" id="holesMain" data-toggle="dropdown" href="#">Hole 2 <img src="/img/arrow-dropdown.png"></a>
                            <ul class="dropdown-menu" role="menu" aria-labelledby="holesMain">
            <li>
                <a href="/colt/hole/hole1.php">Hole 1</a>
            </li>
            <li>
                <a href="/colt/hole/hole2.php">Hole 2</a>
            </li>
            <li>
                <a href="/colt/hole/hole3.php">Hole 3</a>
            </li>
            <li>
                <a href="/colt/hole/hole4.php">Hole 4</a>
            </li>
            <li>
                <a href="/colt/hole/hole5.php">Hole 5</a>
            </li>
            <li>
                <a href="/colt/hole/hole6.php">Hole 6</a>
            </li>
            <li>
                <a href="/colt/hole/hole7.php">Hole 7</a>
            </li>
            <li>
                <a href="/colt/hole/hole8.php">Hole 8</a>
            </li>
            <li>
                <a href="/colt/hole/hole9.php">Hole 9</a>
            </li>
            <li>
                <a href="/colt/hole/hole10.php">Hole 10</a>
            </li>
            <li>
                <a href="/colt/hole/hole11.php">Hole 11</a>
            </li>
            <li>
                <a href="/colt/hole/hole12.php">Hole 12</a>
            </li>
            <li>
                <a href="/colt/hole/hole13.php">Hole 13</a>
            </li>
            <li>
                <a href="/colt/hole/hole14.php">Hole 14</a>
            </li>
            <li>
                <a href="/colt/hole/hole15.php">Hole 15</a>
            </li>
            <li>
                <a href="/colt/hole/hole16.php">Hole 16</a>
            </li>
            <li>
                <a href="/colt/hole/hole17.php">Hole 17</a>
            </li>
            <li>
                <a href="/colt/hole/hole18.php">Hole 18</a>
            </li>
        </ul>
                    </div>

<?php
$pinfo = pathinfo($_SERVER["SCRIPT_FILENAME"]);
$reqpath = dirname($_SERVER["REQUEST_URI"]);

if(preg_match("/(.*?)(\d+)\.php/",  $pinfo["basename"], $matches)) {
    $fnbase = $matches[1];
    $fndir = $pinfo["dirname"];
    $current = intval($matches[2]);
    $next = $current + 1;
    $prior = $current - 1;
    $next_file = $fndir . DIRECTORY_SEPARATOR . $fnbase . $next . ".php";
    $prior_file = $fndir . DIRECTORY_SEPARATOR . $fnbase . $prior . ".php";

    if(!file_exists($next_file)) $next_file = false;
    if(!file_exists($prior_file)) $prior_file = false;


    if($prior_file) {
        $link = $reqpath . DIRECTORY_SEPARATOR . basename($prior_file);

        echo '<a href="'.$link.'" class="btn-new"><img src="/img/prev.png"></a>';
    }

    if($next_file) {
        $link = $reqpath . DIRECTORY_SEPARATOR . basename($next_file);


        echo '<a class="btn-new" href="'.$link.'"><img src="/img/next.png"></a>';
    }
}
?>



                </div>
              </div>

              <div class="row">
                <div class="holedetails">

              <div class="hole_par">
                <h2>Par 4</h2>
                <div class="yards">
                  <div class="yard">
                    <div class="yrdBlack"></div><p>401</p>
                  </div>
                  <div class="yard">
                    <div class="yrdBlue"></div><p>391</p>
                  </div>
                  <div class="yard">
                   <div class="yrdWhite"></div><p>382</p>
                  </div>
                 <div class="yard">
                  <div class="yrdRed"></div><p>376</p>
                </div>
                <div class="yard">
                  <div class="yrdGreen"></div><p>338</p>
                </div>
              </div>
            </div>

            <div class="hidden-desktop mobile-hole">
              <img src="../img/hole02.jpg">
            </div>



            <div class="description">
              <p>Stay between the strategic well-placed bunkers off the tee; but may wish to seek advantage from one or other side of the fairway depending on the pin position. One of the most interesting greens for challenging hole locations.</p>
            </div>


              </div>
            </div>

            <div class="row">

                    <p class="copyright" style="margin-left:30px;">Copyright &copy; 2016 <a target="_blank" href="http://www.golfyardageguides.com/">POWERED BY T2GREEN</a></p>
                    <p class="instructions">
                      <img style="width:20px; height: auto;" src="/img/camera-cursor.png">
                      Hover over a camera point for photos.
                    </p>

            </div>

              </div>
            </div>
    </div>

        <div class="span6">
          <div class="mainarea">
           <div id="tooltip"></div>
              <img name="hole02_icons" src="../img/hole02_icons.jpg" id="cc_hole2" usemap="#m_cc_hole2" alt="" />
      <map name="m_cc_hole2" id="cc_hole2">
        <area shape="poly" class="slidepopup" coords="389,636,421,636,421,657,389,657,389,636" href="../img/slides/02-1.jpg" data-imgsrc="../img/slides/02-1-thumb.jpg"/>
        <area shape="poly" class="slidepopdown" coords="236,250,268,250,268,271,236,271,236,250" href="../img/slides/02-2.jpg" data-imgsrc="../img/slides/02-2-thumb.jpg" />

      </map>
      <div class="cam_information" >
        <!-- POPUP CONTENT -->
        <img src="" alt="" class="holepreview"/><div class="zoom_icon"></div>
      </div>

      <script type="text/javascript">
        var imgArray = ['../img/slides/02-1.jpg', '../img/slides/02-2.jpg'];
      </script>
         </div>
        </div>

      </div>
<?php
   $lead = $_SERVER['DOCUMENT_ROOT'];
   $lead .= "/templates/footer-holes.php";
   include_once($lead);
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/templates/footer.php";
   include_once($path);
?>
