
<div class="row-fluid">
              <div class="hidden-desktop">
              <div class="nav">
                    <a class="btn-new" href="/index.php"><img src="/img/house-icon-small.png"></a>
                    <div class="holesDropdown" style="position:relative;">
                    <a class="btn-new mobilenavbtn" id="holesMain" data-toggle="dropdown" href="#"></a>
                    <?php include_once('dropdown.php'); ?>
                    </div>

<?php
$pinfo = pathinfo($_SERVER["SCRIPT_FILENAME"]);
$reqpath = dirname($_SERVER["REQUEST_URI"]);

if(preg_match("/(.*?)(\d+)\.php/",  $pinfo["basename"], $matches)) {
    $fnbase = $matches[1];
    $fndir = $pinfo["dirname"];
    $current = intval($matches[2]);
    $next = $current + 1;
    $prior = $current - 1;
    $next_file = $fndir . DIRECTORY_SEPARATOR . $fnbase . $next . ".php";
    $prior_file = $fndir . DIRECTORY_SEPARATOR . $fnbase . $prior . ".php";

    if(!file_exists($next_file)) $next_file = false;
    if(!file_exists($prior_file)) $prior_file = false;

    if($prior_file == false) {
        echo '<a class="btn-new inactive"><img src="/img/prev.png"></a>';
    }

    if($prior_file) {
        $link = $reqpath . DIRECTORY_SEPARATOR . basename($prior_file);

        echo '<a href="'.$link.'" class="btn-new"><img src="/img/prev.png"></a>';
    }

    if($next_file) {
        $link = $reqpath . DIRECTORY_SEPARATOR . basename($next_file);


        echo '<a class="btn-new" style="margin-right:0px !important;"  href="'.$link.'"><img src="/img/next.png"></a>';
    }

    if($next_file == false) {
        echo '<a class="btn-new inactive" style="margin-right:0px !important;"><img src="/img/next.png"></a>';
    }
}
?>
</div>

</div>
</div>