<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/templates/header.php";
   include_once($path);
?>


      <div class="row-fluid">

        <div class="span3 hidden-phone hidden-tablet">
            <div class="row-fluid">
              <div class="sidebar logo">
                <a href="/">
                  <img src="/img/toronto-golf.png">
                </a>
              </div>
            </div>

        <div class="bottomnav">
          <div class="sticky">
            <div class="row-fluid buttonnav">
              <div class="sidebar">
                <a class="btn-new" href="/"><img src="/img/main-icon.png">Main Map</a>
              </div>
            </div>

              <div class="row-fluid">
                  <p class="copyright">Copyright &copy; 2016 <a href="#">POWERED BY T2GREEN</a></p>
              </div>
        </div>
      </div>
    </div>

        <div class="span9">
          <div class="mainarea">
            <div class="btmgrad score-header hidden-phone hidden-tablet">
              <h2 style="letter-spacing:2px;">Scorecard</h2>
            </div>

            <div class="score-buttons hidden-phone hidden-tablet">
              <ul>
                <li><a class="btn-new" id="coltbtn">Colt Course</a></li>
                <li><a class="inactive btn-new" id="watsonbtn">Watson Course</a></li>
              </ul>
            </div>

            <div class="hidden-desktop visible-phone visible-tablet phone-header">
             <a class="btn-new scoreback" id="scoresendback" href="#"><img src="/img/prev.png"></a>
                <div class="coursename">
                  <div class="scorewrapper">
                    <h3 id="colthead">Colt Course</h3>
                    <h3 id="watsonhead">Watson Course</h3>
                    <h2 id="scorehead" style="letter-spacing:2px;">Scorecard</h2>
                  </div>
                </div>
            </div>


    <div class="scorecard">
      <table cellpadding="0" class="coltscores">
        <tr class="tableClass-b tableClass-b3">
          <td></td>
          <td><div class="labelBlack"></div></td>
          <td><div class="labelBlue"></div></td>
          <td><div class="labelWhite"></div></td>
          <td><div class="labelRed"></div></td>
          <td><div class="labelGreen"></div></td>
          <td>Par</td>
        </tr>
        <tr class="tableClass-b">
          <td>1</td>
          <td>370</td>
          <td>370</td>
          <td>354</td>
          <td>338</td>
          <td>322</td>
          <td>4</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>2</td>
          <td>401</td>
          <td>391</td>
          <td>382</td>
          <td>376</td>
          <td>338</td>
          <td>4</td>
        </tr>
        <tr class="tableClass-b">
          <td>3</td>
          <td>467</td>
          <td>447</td>
          <td>410</td>
          <td>404</td>
          <td>366</td>
          <td>4/5</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>4</td>
          <td>190</td>
          <td>190</td>
          <td>176</td>
          <td>145</td>
          <td>145</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b">
          <td>5</td>
          <td>476</td>
          <td>449</td>
          <td>424</td>
          <td>403</td>
          <td>379</td>
          <td>4/5</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>6</td>
          <td>377</td>
          <td>377</td>
          <td>347</td>
          <td>327</td>
          <td>327</td>
          <td>4</td>
        </tr>
        <tr class="tableClass-b">
          <td>7</td>
          <td>221</td>
          <td>183</td>
          <td>179</td>
          <td>164</td>
          <td>101</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>8</td>
          <td>406</td>
          <td>406</td>
          <td>398</td>
          <td>385</td>
          <td>312</td>
          <td>4</td>
        </tr>
        <tr class="tableClass-b">
          <td>9</td>
          <td>452</td>
          <td>452</td>
          <td>437</td>
          <td>433</td>
          <td>421</td>
          <td>4/5</td>
        </tr>
        <tr class="tableClass-d">
          <td>Out</td>
          <td>3360</td>
          <td>3265</td>
          <td>3106</td>
          <td>2975</td>
          <td>2711</td>
          <td>34/37</td>
        </tr>
      </table>
      <table cellpadding="0" class="coltscores">
        <tr class="tableClass-b tableClass-b3">
          <td></td>
          <td><div class="labelBlack"></div></td>
          <td><div class="labelBlue"></div></td>
          <td><div class="labelWhite"></div></td>
          <td><div class="labelRed"></div></td>
          <td><div class="labelGreen"></div></td>
          <td>Par</td>
        </tr>
        <tr class="tableClass-b">
          <td>10</td>
          <td>380</td>
          <td>346</td>
          <td>328</td>
          <td>311</td>
          <td>311</td>
          <td>4</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>11</td>
          <td>426</td>
          <td>410</td>
          <td>391</td>
          <td>372</td>
          <td>302</td>
          <td>4</td>
        </tr>
        <tr class="tableClass-b">
          <td>12</td>
          <td>390</td>
          <td>379</td>
          <td>357</td>
          <td>325</td>
          <td>325</td>
          <td>4</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>13</td>
          <td>556</td>
          <td>556</td>
          <td>504</td>
          <td>476</td>
          <td>415</td>
          <td>5</td>
        </tr>
        <tr class="tableClass-b">
          <td>14</td>
          <td>175</td>
          <td>164</td>
          <td>144</td>
          <td>135</td>
          <td>86</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>15</td>
          <td>463</td>
          <td>445</td>
          <td>387</td>
          <td>365</td>
          <td>320</td>
          <td>4</td>
        </tr>
        <tr class="tableClass-b">
          <td>16</td>
          <td>516</td>
          <td>496</td>
          <td>474</td>
          <td>455</td>
          <td>455</td>
          <td>5</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>17</td>
          <td>225</td>
          <td>225</td>
          <td>213</td>
          <td>206</td>
          <td>149</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b">
          <td>18</td>
          <td>345</td>
          <td>345</td>
          <td>327</td>
          <td>303</td>
          <td>266</td>
          <td>4</td>
        </tr>
        <tr class="tableClass-d">
          <td>IN</td>
          <td>3476</td>
          <td>3366</td>
          <td>3125</td>
          <td>2948</td>
          <td>2629</td>
          <td>36</td>
        </tr>
      </table>
      <div class="clearfix"></div>
      <table class="coltSC coltscores" cellpadding="0">
        <tr class="tableClass-d">
          <td>Tot</td>
          <td>6836</td>
          <td>6631</td>
          <td>6231</td>
          <td>5923</td>
          <td>5360</td>
          <td>70/73</td>
        </tr>
      </table>

      <table style="display:none;" cellpadding="0" class="watsonscores">
        <tr class="tableClass-b tableClass-b3">
          <td></td>
          <td><div class="labelBlue"></div></td>
          <td><div class="labelWhite"></div></td>
          <td><div class="labelRed"></div></td>
          <td><div class="labelYellow"></div></td>
          <td>PAR</td>
        </tr>
        <tr class="tableClass-b">
          <td>1</td>
          <td>376</td>
          <td>326</td>
          <td>302</td>
          <td>228</td>
          <td>4</td>

        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>2</td>
          <td>146</td>
          <td>135</td>
          <td>92</td>
          <td>92</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b">
          <td>3</td>
          <td>299</td>
          <td>290</td>
          <td>261</td>
          <td>190</td>
          <td>4</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>4</td>
          <td>144</td>
          <td>120</td>
          <td>103</td>
          <td>103</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b">
          <td>5</td>
          <td>204</td>
          <td>171</td>
          <td>133</td>
          <td>133</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>6</td>
          <td>179</td>
          <td>162</td>
          <td>86</td>
          <td>86</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b">
          <td>7</td>
          <td>358</td>
          <td>314</td>
          <td>308</td>
          <td>159</td>
          <td>4</td>
        </tr>
        <tr class="tableClass-b tableClass-b2">
          <td>8</td>
          <td>121</td>
          <td>110</td>
          <td>100</td>
          <td>42</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-b">
          <td>9</td>
          <td>185</td>
          <td>175</td>
          <td>164</td>
          <td>127</td>
          <td>3</td>
        </tr>
        <tr class="tableClass-d">
          <td>Tot</td>
          <td>2012</td>
          <td>1803</td>
          <td>1549</td>
          <td>1160</td>
          <td>30</td>
        </tr>
      </table>


    </div>
          </div>
        </div>

      </div>

      <script>
$(document).ready(function() {

    var referrer =  document.referrer;

    if((referrer.toLowerCase().indexOf("colt") !== -1) || (window.location.href.indexOf("colt") !== -1)){

                       $(".watsonscores").hide();
                       $( "#watsonbtn" ).addClass( "inactive" );
                       $( "#coltbtn" ).removeClass( "inactive" );
                       $("#watsonhead").css('display','none');
                       $("#colthead").css('display','block');
                       $("#scoresendback").attr("href", "http://torontogolf.coursetour.ca/colt/");

        } else if ((referrer.toLowerCase().indexOf("watson") !== -1) || (window.location.href.indexOf("watson") !== -1)){
                      $(".coltscores").hide();
                       $( "#coltbtn" ).addClass( "inactive" );
                       $( "#watsonbtn" ).removeClass( "inactive" );
                       $(".watsonscores").css('display','block');
                       $("#colthead").css('display','none');
                       $("#watsonhead").css('display','block');
                       $("#scoresendback").attr("href", "http://torontogolf.coursetour.ca/watson/");

        }
})
</script>

<?php
   $path = $_SERVER['DOCUMENT_ROOT'];
   $path .= "/templates/footer.php";
   include_once($path);
?>
